new Vue({
  el: '#app',
  data: {
    map: null,
    tileLayer: null,
    layers: [
      {
        id: 0,
        name: 'Restaurante',
        active: false,
        features: [
          {
            id: 0,
            name: 'Bar 1',
            type: 'marker',
            coords: [43.81903, 28.58542],
          },
          {
            id: 1,
            name: 'Bar 2',
            type: 'marker',
            coords: [43.81836, 28.58711],
          },
          {
            id: 2,
            name: 'Ba 3',
            type: 'marker',
            coords: [43.81614, 28.58563],
          },

        ],
      },
      {
        id: 1,
        name: 'Limite',
        active: false,
        features: [
          {
            id: 0,
            name: 'Mangalia',
            type: 'polygon',
            coords: [
				[28.59187603, 43.81334865],
				[28.59187603, 43.80867235],
				[28.5783577, 43.80761936],
				[28.5733366, 43.81381317],
				[28.57595444, 43.81879871],
				[28.58131886, 43.82403154],
				[28.59142542, 43.82465078],
				[28.59118938, 43.81886611],
				[28.59264851, 43.81573312],
				[28.59187603, 43.81334865],
            ],
          }
        ],
      }
    ],
  },
  mounted() {
    this.initMap();
    this.initLayers();
  },
  methods: {
    layerChanged(layerId, active) {
      const layer = this.layers.find(layer => layer.id === layerId);
      
      layer.features.forEach((feature) => {
        if (active) {
          feature.leafletObject.addTo(this.map);
        } else {
          feature.leafletObject.removeFrom(this.map);
        }
      });
    },
    initLayers() {
      this.layers.forEach((layer) => {
        const markerFeatures = layer.features.filter(feature => feature.type === 'marker');
        const polygonFeatures = layer.features.filter(feature => feature.type === 'polygon');
        
        markerFeatures.forEach((feature) => {
          feature.leafletObject = L.marker(feature.coords)
            .bindPopup(feature.name);
        });
        
        polygonFeatures.forEach((feature) => {
          feature.leafletObject = L.polygon(feature.coords)
            .bindPopup(feature.name);
        });
      });
    },
    initMap() {
      this.map = L.map('map').setView([43.8167, 28.5824], 16);
      this.tileLayer = L.tileLayer(
        'https://cartodb-basemaps-{s}.global.ssl.fastly.net/rastertiles/voyager/{z}/{x}/{y}.png',
        {
          maxZoom: 18,
          attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, &copy; <a href="https://carto.com/attribution">CARTO</a>',
        }
      );
        
      this.tileLayer.addTo(this.map);
    },
  },
});
